angular.module('starter.controllers', [])

    .controller('EmoListCtrl', [
        '$scope',
        'Emotions',
        'UserEmotions',
        function ($scope,
                  Emotions,
                  UserEmotions) {
            $scope.emotree = Emotions.getBaseEmoTree();
            for (var i = 1; i < $scope.emotree.length; i++) {
                $scope.emotree.show = false;
            }

            $scope.toggleShow = function (bi) {
                bi.show = !bi.show;
            }

            $scope.setState = function (emo, statecode) {
                return UserEmotions.setState(emo, statecode);
            }

            $scope.isState = function (emo, statecode) {
                return UserEmotions.isState(emo, statecode);
            }


        }])

    .controller('EmoStatusCtrl', [
        '$scope',
        'UserEmotions',
        function ($scope,
                  UserEmotions) {
            $scope.emostates = UserEmotions.get();


            $scope.setState = function (emo, statecode) {
                return UserEmotions.setState(emo, statecode);
            }

            $scope.isState = function (emo, statecode) {
                return UserEmotions.isState(emo, statecode);
            }

            $scope.saveState = function () {

            }

        }])


    .controller('AccountCtrl', function ($scope) {
        $scope.settings = {
            enableFriends: true
        };
    });
