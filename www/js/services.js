angular.module('starter.services', [])


.factory('Emotions', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var emotions = [{ basic_emo_id:0,
    id: 0,
    name: 'Гнев',
  }, {
    id: 1,basic_emo_id:0,
    name: 'Бешенство',
  }, {
    id: 2,basic_emo_id:0,
    name: 'Ярость',
  }, {
    id: 3,basic_emo_id:0,
    name: 'Ненавить',
  }, {
    id: 4,basic_emo_id:0,
    name: 'Истерия',
  }, {
      id: 5,basic_emo_id:0,
      name: 'Злость',
  }, {
      id: 6,basic_emo_id:0,
      name: 'Раздражение',
  }, {
      id: 7,basic_emo_id:0,
      name: 'Презрение',
  }, {
      id: 8,basic_emo_id:0,
      name: 'Негодование',
  }, {
      id: 9,basic_emo_id:0,
      name: 'Обида',
  }, {
      id: 10,basic_emo_id:0,
      name: 'Ревность',
  }, {
      id: 11,basic_emo_id:0,
      name: 'Уязвленность',
  }, {
      id: 12,basic_emo_id:0,
      name: 'Досада',
  }, {
      id: 13,basic_emo_id:0,
      name: 'Зависть',
  }, {
      id: 14,basic_emo_id:0,
      name: 'Неприязнь',
  }, {
      id: 15,basic_emo_id:0,
      name: 'Возмущение',
  }, {
      id: 16,basic_emo_id:0,
      name: 'Отвращение',
  }, {
      id: 17,basic_emo_id:1,
      name: 'Страх',
  }, {
      id: 18,basic_emo_id:1,
      name: 'Ужас',
  }, {
      id: 19,basic_emo_id:1,
      name: 'Отчаяние',
  }, {
      id: 20,basic_emo_id:1,
      name: 'Испуг',
  }, {
      id: 21,basic_emo_id:1,
      name: 'Оцепенение',
  }, {
      id: 22,basic_emo_id:1,
      name: 'Подозрение',
  }, {
      id: 23,basic_emo_id:1,
      name: 'Тревога',
  },
      {
          id: 24,basic_emo_id:1,
          name: 'Ошарашенность',
      }, {
          id: 25,basic_emo_id:1,
          name: 'Беспокойство',
      }, {
          id: 26,basic_emo_id:1,
          name: 'Боязнь',
      }, {
          id: 27,basic_emo_id:1,
          name: 'Унижение',
      }, {
          id: 28,basic_emo_id:1,
          name: 'Замешательство',
      }, {
          id: 29,basic_emo_id:1,
          name: 'Растерянность',
      }, {
          id: 30,basic_emo_id:1,
          name: 'Вина',
      }, {
          id: 31,basic_emo_id:1,
          name: 'Стыд',
      }, {
          id: 32,basic_emo_id:1,
          name: 'Сомнение',
      }, {
          id: 33,basic_emo_id:1,
          name: 'Застенчивость',
      }, {
          id: 34,basic_emo_id:1,
          name: 'Опасение',
      }, {
          id: 35,basic_emo_id:1,
          name: 'Смущение',
      }, {
          id: 36,basic_emo_id:1,
          name: 'Сломленность',
      }, {
          id: 37,basic_emo_id:1,
          name: 'Подвох',
      }, {
          id: 38,basic_emo_id:1,
          name: 'Надменность',
      }, {
          id: 39,basic_emo_id:1,
          name: 'Ошеломленность',
      }, {
          id: 40,basic_emo_id:2,
          name: 'Грусть',
      }, {
          id: 41,basic_emo_id:2,
          name: 'Горечь',
      }, {
          id: 42,basic_emo_id:2,
          name: 'Тоска',
      }, {
          id: 43,basic_emo_id:2,
          name: 'Скорбь',
      }, {
          id: 44,basic_emo_id:2,
          name: 'Лень',
      }, {
          id: 45,basic_emo_id:2,
          name: 'Жалость',
      }, {
          id: 46,basic_emo_id:2,
          name: 'Отрешенность',
      }, {
          id: 47,basic_emo_id:2,
          name: 'Отчаяние',
      }, {
          id: 48,basic_emo_id:2,
          name: 'Беспомощность',
      }, {
          id: 49,basic_emo_id:2,
          name: 'Душевная боль',
      }, {
          id: 50,basic_emo_id:2,
          name: 'Безнадежность',
      }, {
          id: 51,basic_emo_id:2,
          name: 'Отчуждение',
      }, {
          id: 52,basic_emo_id:2,
          name: 'Разочарование',
      }, {
          id: 53,basic_emo_id:2,
          name: 'Потрясение',
      }, {
          id: 54,basic_emo_id:2,
          name: 'Сожаление',
      }, {
          id: 55,basic_emo_id:2,
          name: 'Скука',
      }, {
          id: 56,basic_emo_id:2,
          name: 'Безысходность',
      }, {
          id: 57,basic_emo_id:2,
          name: 'Печаль',
      },{
          id: 58,basic_emo_id:2,
          name: 'Загнанность',
      },{
          id: 59,basic_emo_id:3,
          name: 'Счастье',
      },{
          id: 101,basic_emo_id:3,
          name: 'Радость',
      },
      {
          id: 60,basic_emo_id:3,
          name: 'Вострог',
      },{
          id: 61,basic_emo_id:3,
          name: 'Ликование',
      },{
          id: 62,basic_emo_id:3,
          name: 'Приподнятость',
      },{
          id: 63,basic_emo_id:3,
          name: 'Оживление',
      },{
          id: 64,basic_emo_id:3,
          name: 'Умиротворение',
      },{
          id: 65,basic_emo_id:3,
          name: 'Увлечение',
      },{
          id: 66,basic_emo_id:3,
          name: 'Интерес',
      },{
          id: 67,basic_emo_id:3,
          name: 'Забота',
      },{
          id: 68,basic_emo_id:3,
          name: 'Ожидание',
      },{
          id: 69,basic_emo_id:3,
          name: 'Возбуждение',
      },{
          id: 70,basic_emo_id:3,
          name: 'Предвкушение',
      },{
          id: 71,basic_emo_id:3,
          name: 'Надежда',
      },{
          id: 72,basic_emo_id:3,
          name: 'Любопыство',
      },{
          id: 73,basic_emo_id:3,
          name: 'Освобождение',
      },{
          id: 74,basic_emo_id:3,
          name: 'Принятие',
      },{
          id: 75,basic_emo_id:3,
          name: 'Нетерпение',
      },{
          id: 76,basic_emo_id:3,
          name: 'Вера',
      },{
          id: 77,basic_emo_id:3,
          name: 'Изумление',
      },{
          id: 78,basic_emo_id:4,
          name: 'Любовь',
      },{
          id: 79,basic_emo_id:4,
          name: 'Нежность',
      },{
          id: 80,basic_emo_id:4,
          name: 'Теплота',
      },{
          id: 81,basic_emo_id:4,
          name: 'Сочувствие',
      },{
          id: 82,basic_emo_id:4,
          name: 'Блаженство',
      },{
          id: 83,basic_emo_id:4,
          name: 'Доверие',
      },{
          id: 84,basic_emo_id:4,
          name: 'Безопасность',
      },{
          id: 85,basic_emo_id:4,
          name: 'Благодарность',
      },{
          id: 86,basic_emo_id:4,
          name: 'Спокойствие',
      },{
          id: 87,basic_emo_id:4,
          name: 'Симпатия',
      },{
          id: 88,basic_emo_id:4,
          name: 'Идентичность',
      },{
          id: 89,basic_emo_id:4,
          name: 'Гордость',
      },
      {
          id: 90,basic_emo_id:4,
          name: 'Восхищение',
      },{
          id: 91,basic_emo_id:4,
          name: 'Уважение',
      },{
          id: 92,basic_emo_id:4,
          name: 'Самоценность',
      },{
          id: 93,basic_emo_id:4,
          name: 'Влюбленность',
      },{
          id: 94,basic_emo_id:4,
          name: 'Любовь к себе',
      },{
          id: 95,basic_emo_id:4,
          name: 'Очарованность',
      },{
          id: 96,basic_emo_id:4,
          name: 'Смирение',
      },{
          id: 97,basic_emo_id:4,
          name: 'Искренность',
      },{
          id: 98,basic_emo_id:4,
          name: 'Дружелюбие',
      },{
          id: 99,basic_emo_id:4,
          name: 'Доброта',
      },{
          id: 100,basic_emo_id:4,
          name: 'Взаимовыручка',
      },  {        id:101,
            name: ' Оценки'
    },
    {
        id:102,basic_emo_id:5,
        name: 'Ошибки'
    },
    {
        id:103,basic_emo_id:5,
        name: 'Нового'
    },
    {
        id:104,basic_emo_id:5,
        name: 'Одиночества'
    },
    {
        id:105,basic_emo_id:5,
        name: 'Отвествености'
    },
    {
        id:106,basic_emo_id:5,
        name: 'Темноты'
    },
    {
        id:107,basic_emo_id:5,
        name: 'Высоты'
    },
    {
        id:108,basic_emo_id:5,
        name: 'Разочарования в себе'
    },  {
        id:109,basic_emo_id:5,
        name: 'Будущего'
    },  {
        id:110,basic_emo_id:5,
        name: 'За свою жизнь'
    }


];

  var states = [
      { id:0,
          name: 'Нервозность',basic_emo_id:0,
      },
      { id:1,
          name: 'Пренебрежение',basic_emo_id:0,
      },
      { id:2,
          name: 'Недовольство',basic_emo_id:0,
      },
      { id:3,
          name: 'Вредность',basic_emo_id:0,
      },
      { id:4,
          name: 'Огорчение',basic_emo_id:0,
      },
      { id:5,
          name: 'Нетерпимость',basic_emo_id:0,
      },
      { id:6,
          name: 'Вседозволенность',basic_emo_id:0,
      },
      { id:7,
          name: 'Раскаяние',basic_emo_id:1,
      },
      { id:8,
          name: 'Безысходность',basic_emo_id:1,
      },
      { id:9,
          name: 'Превосходство',basic_emo_id:1,
      },
      { id:10,
          name: 'Высокомерие',basic_emo_id:1,
      },
      { id:11,
          name: 'Высокомерие',basic_emo_id:1,
      },
      { id:12,
          name: 'Неполноценность',basic_emo_id:1,
      },
      { id:13,
          name: 'Неудобство',basic_emo_id:1,
      },
      { id:14,
          name: 'Неловкость',basic_emo_id:1,
      },
      { id:15,
          name: 'Апатия',basic_emo_id:1,
      },
      { id:16,
          name: 'Безразличие',basic_emo_id:1,
      },
      { id:17,
          name: 'Неуверенность',basic_emo_id:1,
      },
      { id:18,
          name: 'Напряженность',basic_emo_id:1,
      },
      { id:19,
          name: 'Тупик',basic_emo_id:2,
      },
      { id:20,
          name: 'Усталость',basic_emo_id:2,
      },
      { id:21,
          name: 'Принуждение',basic_emo_id:2,
      },
      { id:22,
          name: 'Одиночество',basic_emo_id:2,
      },
      { id:23,
          name: 'Отверженность',basic_emo_id:2,
      },   { id:24,
          name: 'Подавленность',basic_emo_id:2,
      },   { id:25,
          name: 'Холодность',basic_emo_id:2,
      },   { id:26,
          name: 'Безучастность',basic_emo_id:2,
      },   { id:27,
          name: 'Равнодушие',basic_emo_id:2,
      },   { id:28,
          name: 'Удовлетворение',basic_emo_id:3,
      },   { id:29,
          name: 'Уверенность',basic_emo_id:3,
      },   { id:30,
          name: 'Удовольствие',basic_emo_id:3,
      },   { id:31,
          name: 'Окрыленность',basic_emo_id:3,
      },   { id:32,
          name: 'Торжественность',basic_emo_id:3,
      },   { id:33,
          name: 'Жизнерадостьность',basic_emo_id:3,
      },   { id:34,
          name: 'Облегчение',basic_emo_id:3,
      },   { id:35,
          name: 'Ободренность',basic_emo_id:3,
      },   { id:36,
          name: 'Удивление',basic_emo_id:3,
      }, { id:37,
          name: 'Сопереживание',basic_emo_id:4,
      }, { id:38,
          name: 'Сопричастность',basic_emo_id:4,
      }, { id:39,
          name: 'Уравновешенность',basic_emo_id:4,
      }, { id:40,
          name: 'Смирение',basic_emo_id:4,
      }, { id:50,
          name: 'Естественность',basic_emo_id:4,
      }, { id:51,
          name: 'Жизнелюбие',basic_emo_id:4,
      }, { id:52,
          name: 'Вдохновение',basic_emo_id:4,
      }, { id:53,
          name: 'Воодушевление',basic_emo_id:4,
      },

  ];



  var basic_emos = [
      {
        id: 0,
        name : 'Гнев',

      },
      {id: 1,
          name : 'Страх',


      },
      {id: 2,
          name : 'Грусть',


      },
      {id: 3,
          name : 'Радость',


      },
      {id: 4,
          name : 'Любовь',

      },{
      id: 5,
          name: 'Страх по типам'
      }


  ];

    var fears = [

    ]



    var current_state = {
        emos : [],
        comment: '',
        date: new Date(),
    };


    var states_saved = [];




    var getEmoByBasicId =  function (bid)  {
        var emobid = [];
        for (var i=0; i<emotions.length; i++) {
            if (emotions[i].basic_emo_id == bid) {
                emotions[i].type = 'emotion';
                emobid.push (emotions[i]);
            }
        }
        for (i=0; i<states; i++) {
            if (states[i].basic_emo_id == bid) {
                states[i].type = 'state';
                emobid.push (states[i]);
            }
        }
        return emobid;
    }


    function range (start, count) {
        return Array.apply(0, Array(count))
            .map(function (element, index) {
                return index + start;
            });
    }



  return {
    all: function() {
      return emotions;
    },
    remove: function(emotion) {
      emotions.splice(emotions.indexOf(emotion), 1);
    },
    get: function(emotionId) {
      for (var i = 0; i < emotions.length; i++) {
        if (emotions[i].id === parseInt(emotionId)) {
          return emotions[i];
        }
      }
        return null;
    },


      getBaseEmoTree: function () {
             var tree = [];
            for (var i =0; i<basic_emos.length; i++) {
                var be = angular.copy(basic_emos[i]);
                be.emos = getEmoByBasicId(be.id);
                tree.push(be);
            }
            return tree;

      }




  };
})


.service ('UserEmotions', [ function () {

    var emostates = [];

    this.setState = function (emotion, state) {
        console.log ('setState', emotion, state);
        console.log (emostates);

        for (var i=0; i<emostates.length; i++) {
            if (emostates[i].id == emotion.id) {
                if (this.isState (emotion, state) ) {
                    emostates.splice(i,1);
                    return;
                }
                emostates[i].state=state;
                return;
            }
        }
        emotion.state = state;
        emostates.push (emotion);

    }

    this.isState = function (emotion, state) {

        for (var i=0; i<emostates.length; i++) {
            if (emostates[i].id == emotion.id && emotion.type == emostates[i].type) {
                if (emotion.state == state) {
                    console.log ('isState','yes');
                    return true;
                }
            }
        }
        return false;
    }
    this.get = function () {
        return emostates;
    }

}])
    .service ('StatesService', [ function () {

        var states = [
            {
                id: 0,
                date: new Date(),
                emotions: []
             }
        ]
        

    }])



;
